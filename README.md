# thinkjohn-vso

This is a repository for me (John Young) to practice creating and configuring cloud-based development environments using [Visual Studio Online](https://docs.microsoft.com/en-us/visualstudio/online/overview/what-is-vsonline).  

Mostly, I'm using these environments for development on the [Pixelbook Go](https://store.google.com/us/product/pixelbook_go) I'm using as my main computer in February 2020.

## What I'm hoping to learn

### Installation and configuration

- [x] How to use `devcontainer.json` and a Dockerfile to configure a cloud-based dev environment.
- [ ] Whether you can configure an environment only once at creation, or whether changes to the devcontainer and dockerfile are applied to an existing environment.
- [ ] How to use dotfiles to synchronize my preferences across cloud-based dev environments.

### Workflows, sharing, and gotchas

- [ ] What the quirks are of using Visual Studio Code in a web browser. (For instance, it's not `Ctrl-S` to save, it seems to be `Ctrl-Shift-S`.)
- [ ] Whether and how to share dev environments between users.  Does one only share the repo with the configuration files so another user can build an identical instance to yours, or can you share access to the _actual environment_ you have created?
- [ ] How to use the "[Live share](https://visualstudio.microsoft.com/services/live-share/)" features of Visual Studio Online.

## Useful Links

A general explainer from Microsoft: [Developing with Visual Studio Online](https://docs.microsoft.com/en-us/visualstudio/online/reference/configuring)

There are a number of sample Dockerfiles for different configurations in the [vscode-dev-containers repository](https://github.com/Microsoft/vscode-dev-containers/tree/master/containers).

Microsoft has made a number of full "try it out" repositories [on Github](https://github.com/search?q=org%3Amicrosoft+vscode-remote-try-&type=Repositories)